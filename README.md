# README #

### How do I get set up? ###

* MySQL database and user
```
#!SQL
CREATE DATABASE JSFCompositeComponents DEFAULT CHARSET UTF8;

CREATE USER 'test'@'localhost' IDENTIFIED BY 'test';
GRANT ALL ON JSFCompositeComponents.* TO 'test'@'localhost';
```

* run maven command

```
#!bash
mvn clean install tomcat7:run
```

* open in browser
[localhost:8080/GUIEditorJSF/](http://localhost:8080/GUIEditorJSF/)