package ch.ultrasoft.guieditorjsf.converter;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;

import ch.ultrasoft.guieditorjsf.viewmodel.AbstractViewElement;
import ch.ultrasoft.guieditorjsf.xmlservice.ViewElementService;

@SessionScoped
@Named("viewModelConverter")
public class ViewModelConverter implements Converter, Serializable {

	private static final long serialVersionUID = 1L;
	private ViewElementService service = new ViewElementService();

	@Override
	public Object getAsObject(FacesContext context, UIComponent uiComponent, String value) {
		
		try {
			return service.findViewElement(Long.valueOf(value));	
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent uiComponent, Object value) {
		return value == null ? null : ((AbstractViewElement) value).getId().toString();
	}

}
