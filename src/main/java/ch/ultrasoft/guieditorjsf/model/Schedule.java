/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.ultrasoft.guieditorjsf.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kenny
 */
@Entity
@Table(name = "SCHEDULE")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Schedule.findAll", query = "SELECT s FROM Schedule s") })
public class Schedule implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ID_SCHEDULE")
	private Integer id;
	@Size(max = 100)
	@Column(name = "NAME")
	private String name;
	@Column(name = "START")
	@Temporal(TemporalType.TIMESTAMP)
	private Date start;
	@Column(name = "END")
	@Temporal(TemporalType.TIMESTAMP)
	private Date end;
	@Size(max = 1000)
	@Column(name = "DESCRIPTION")
	private String description;
	@ManyToOne
	@JoinColumn(name = "id_juser")
	private User user;

	// private Date startDate;
	// private Date endDate;
	// private String title;

	public Schedule() {
	}

	public Schedule(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Date getStartDate() {
		return start;
	}

	public void setStartDate(Date startDate) {
		this.start = startDate;
	}

	public Date getEndDate() {
		return end;
	}

	public void setEndDate(Date endDate) {
		this.end = endDate;
	}

	public String getTitle() {
		return name;
	}

	public void setTitle(String title) {
		this.name = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Schedule [id=" + id + "]";
	}
}
