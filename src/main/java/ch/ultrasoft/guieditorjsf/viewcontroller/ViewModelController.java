package ch.ultrasoft.guieditorjsf.viewcontroller;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import ch.ultrasoft.guieditorjsf.viewmodel.AbstractViewElement;

/**
 * @author manigley
 *
 */
@SessionScoped
@Named("ViewModelController")
public class ViewModelController extends AbstractViewController {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(ViewModelController.class);
	
	private Stack<AbstractViewElement> viewElementsPrevStack = new Stack<AbstractViewElement>();
	private Stack<AbstractViewElement> viewElementsNextStack = new Stack<AbstractViewElement>();
	private String classNameNewElement = null;
	private AbstractViewElement nestedElement = null;

	public ViewModelController() {
	}

	public Set<AbstractViewElement> getViewElements() {
		return service.getViewElements();
	}

	@Override
	public void setSelectedViewElement(AbstractViewElement selectedViewElement) {
		if (getSelectedViewElement() != null)
			viewElementsPrevStack.add(getSelectedViewElement());
		super.setSelectedViewElement(selectedViewElement);
	}

	public void setPreviousElement() {
		if (getSelectedViewElement() != null
				&& (viewElementsNextStack.isEmpty() || viewElementsNextStack.peek() != getSelectedViewElement()))
			viewElementsNextStack.push(getSelectedViewElement());
		try {
			super.setSelectedViewElement(refreshAbstractViewElement(viewElementsPrevStack.pop()));
		} catch (Exception e) {
			LOGGER.error("Next viewElement could not been merged", e);
			setPreviousElement();
		}
	}

	public void setNextElement() {
		if (getSelectedViewElement() != null && getSelectedViewElement().getId() != null
				&& !viewElementsPrevStack.isEmpty() && viewElementsPrevStack.peek() != getSelectedViewElement())
			viewElementsPrevStack.push(getSelectedViewElement());
		try {
			super.setSelectedViewElement(refreshAbstractViewElement(viewElementsNextStack.pop()));
		} catch (Exception e) {
			LOGGER.error("Next view Element could not been merged", e);
			setNextElement();
		}
	}

	private AbstractViewElement refreshAbstractViewElement(AbstractViewElement viewElement) {
		if (viewElement != null && viewElement.getId() != null)
			return service.findViewElement(viewElement.getId());
		return viewElement;
	}

	public void addNestedElement() {
		System.out.println("nestedElement: " + nestedElement + " - " + classNameNewElement);
		setMasterViewElement(getSelectedViewElement());
		if (nestedElement != null) {
			LOGGER.info("addNestedElement: " + nestedElement + "( Master: " + getSelectedViewElement() + ")");
			
			if (getSelectedViewElement().getNestedElements() == null)
				getSelectedViewElement().setNestedElements(new ArrayList<Long>());
			
			getSelectedViewElement().getNestedElements().add(nestedElement.getId());
			setSelectedViewElement(service.putViewElement(getSelectedViewElement()));
		} else
			createAbstractViewElement();
	}

	/**
	 * returns a list by instance variable: classNameNewElement
	 * 
	 * @return
	 */
	public Set<AbstractViewElement> getViewElementsByClassName() {
		Set<AbstractViewElement> abstractViewElements = new HashSet<>();
		if (classNameNewElement != null && !classNameNewElement.isEmpty()) {
			abstractViewElements.addAll(service.findViewElementByClass(classNameNewElement));
			if (abstractViewElements.isEmpty() && LOGGER.isInfoEnabled())
				LOGGER.info("getViewElementsByClassName is Empty");
		}
		return abstractViewElements;
	}

	public void createAbstractViewElement() {
		try {
			Class<?> c = Class.forName(classNameNewElement);
			if (c.getSuperclass() == AbstractViewElement.class) {
				setSelectedViewElement((AbstractViewElement) c.newInstance());
			} else
				throw new AccessDeniedException("Class " + classNameNewElement + "is not allowed as nestedElement");
		} catch (ClassNotFoundException | AccessDeniedException | InstantiationException | IllegalAccessException e) {
			LOGGER.error("Faild to createNestedElement as: " + classNameNewElement, e);
		}
		classNameNewElement = null;
	}

	public void cancelUpdate() {
		nestedElement = null;
	}

	public boolean isPrevStackEmpty() {
		return viewElementsPrevStack.isEmpty();
	}

	public boolean isNextStackEmpty() {
		return viewElementsNextStack.isEmpty();
	}

	public String getClassNameNewElement() {
		return classNameNewElement;
	}

	public void setClassNameNewElement(String classNameNewElement) {
		this.classNameNewElement = classNameNewElement;
	}

	public AbstractViewElement getNestedElement() {
		return nestedElement;
	}

	public void setNestedElement(AbstractViewElement nestedElement) {
		this.nestedElement = nestedElement;
	}
}
