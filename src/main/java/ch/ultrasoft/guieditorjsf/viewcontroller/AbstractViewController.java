package ch.ultrasoft.guieditorjsf.viewcontroller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.el.ELContext;
import javax.el.MethodNotFoundException;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import ch.ultrasoft.guieditorjsf.viewmodel.AbstractViewElement;
import ch.ultrasoft.guieditorjsf.xmlservice.ViewElementService;

public class AbstractViewController implements Serializable {

	private static final long serialVersionUID = 1L;
	private final static Logger LOGGER = Logger.getLogger(AbstractViewController.class);
	private AbstractViewElement masterViewElement = null;

	protected final ViewElementService service = new ViewElementService();

	private AbstractViewElement selectedViewElement;
	private List<AbstractViewElement> selectedViewElementsNestedElements = new ArrayList<>();

	/**
	 * @info returns a Bean by passing her name "@name("xyz")"
	 * @param beanName
	 * @return
	 * @see http://www.javacodegeeks.com/2012/04/5-useful-methods-jsf-
	 *      developers-should.html
	 */
	public static Object getManagedBean(final String beanName) {
		FacesContext fc = FacesContext.getCurrentInstance();
		Object bean;
		try {
			ELContext elContext = fc.getELContext();
			bean = elContext.getELResolver().getValue(elContext, null, beanName);
		} catch (MethodNotFoundException e) {
			LOGGER.error("Bean not found: " + beanName);
			throw new FacesException(e.getMessage(), e);
		}
		if (bean == null) {
			throw new FacesException("Managed bean with name '" + beanName
					+ "' was not found. Check your faces-config.xml or @ManagedBean annotation.");
		}
		return bean;
	}

	protected AbstractViewElement saveViewElement(AbstractViewElement viewElement) throws Exception {
		viewElement = service.putViewElement(viewElement);
		return viewElement;
	}

	public void saveSelectedViewElement() {
		try {
			selectedViewElement = saveViewElement(selectedViewElement);

			if (getMasterViewElement() != null) {
				if (getMasterViewElement().getNestedElements() == null)
					getMasterViewElement().setNestedElements(new ArrayList<Long>());

				getMasterViewElement().getNestedElements().remove(selectedViewElement.getId());
				getMasterViewElement().getNestedElements().add(selectedViewElement.getId());
				setMasterViewElement(service.putViewElement(getMasterViewElement()));
			}

			printFacesMessage("Successfully saved", selectedViewElement.getClass().getSimpleName(),
					FacesMessage.SEVERITY_INFO);
		} catch (Exception e) {
			LOGGER.error("Could not save ViewElement", e);
			printFacesMessage("Faild to save", selectedViewElement.getClass().getSimpleName(),
					FacesMessage.SEVERITY_ERROR);
		}
	}

	public void deleteViewElement(AbstractViewElement viewElement) throws Exception {
		service.deleteViewModel(viewElement);
	}

	public void deleteSelectedViewElement() {
		try {
			String temClassName = selectedViewElement.getClass().getSimpleName();
			deleteViewElement(selectedViewElement);
			printFacesMessage("Successfully deleted", temClassName, FacesMessage.SEVERITY_INFO);
		} catch (Exception e) {
			LOGGER.error("Could not delete ViewElement", e);
			printFacesMessage("Faild to save", selectedViewElement.getClass().getSimpleName(),
					FacesMessage.SEVERITY_ERROR);
		}
	}

	private void initSelectedViewElementsNestedElements() {
		
		selectedViewElementsNestedElements = new ArrayList<AbstractViewElement>();
		
		if (selectedViewElement != null && selectedViewElement.getNestedElements() != null) {
			for (Long l : selectedViewElement.getNestedElements()) {
				selectedViewElementsNestedElements.add(service.findViewElement(l));
			}
		}
	}

	/**
	 * only removes from the nestedElementList. ViewElement will still stay in
	 * xml
	 */
	public void removeNestedElement(AbstractViewElement viewElement) {
		getSelectedViewElement().getNestedElements().remove(viewElement.getId());
		saveSelectedViewElement();
	}

	protected void printFacesMessage(String summary, String detail, Severity severity) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(severity, summary, detail));
	}

	public AbstractViewElement getSelectedViewElement() {
		return selectedViewElement;
	}

	public void setSelectedViewElement(AbstractViewElement selectedViewElement) {
		this.selectedViewElement = selectedViewElement;
		initSelectedViewElementsNestedElements();
	}

	public List<AbstractViewElement> getSelectedViewElementsNestedElements() {
		return selectedViewElementsNestedElements;
	}

	public AbstractViewElement getMasterViewElement() {
		return masterViewElement;
	}

	public void setMasterViewElement(AbstractViewElement masterViewElement) {
		this.masterViewElement = masterViewElement;
	}

	public ViewElementService getService() {
		return service;
	}
}
