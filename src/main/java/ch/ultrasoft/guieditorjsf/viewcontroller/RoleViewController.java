package ch.ultrasoft.guieditorjsf.viewcontroller;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import ch.ultrasoft.guieditorjsf.viewmodel.UltraDialogModel;
import ch.ultrasoft.guieditorjsf.viewmodel.UltraTableModel;

@SessionScoped
@Named("roleViewController")
public class RoleViewController extends AbstractViewController {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(RoleViewController.class);

	public RoleViewController() {

	}

	public UltraTableModel getTableModel() {
		return (UltraTableModel) getService().findViewElement(new Long(7));
	}

	public UltraDialogModel getDialogModel() {
		return (UltraDialogModel) getService().findViewElement(new Long(10));
	}
}
