/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.ultrasoft.guieditorjsf.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import ch.ultrasoft.guieditorjsf.model.Role;

/**
 *
 * @author kenny
 */
@Named("roleController")
@SessionScoped
public class RoleController extends AbstractController<Role> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public RoleController(){
		super(Role.class);
	}
	
	//@TestAnnotation
	public void test() {
		printFacesMessage("Hello", "this is a test");
	}
}