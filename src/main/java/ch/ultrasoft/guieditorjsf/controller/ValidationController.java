package ch.ultrasoft.guieditorjsf.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.log4j.Logger;

@Named("validationController2")
@SessionScoped
public class ValidationController implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(ValidationController.class);

	public void save() {
		LOGGER.debug("save() is called");
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Hello from bean");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void delete() {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Hello from bean");
		FacesContext.getCurrentInstance().addMessage(null, msg);
		LOGGER.debug("doIt() is called");
	}
}
