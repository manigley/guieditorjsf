package ch.ultrasoft.guieditorjsf.xmlservice;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;

import ch.ultrasoft.guieditorjsf.viewmodel.AbstractViewElement;
import ch.ultrasoft.guieditorjsf.xmltransformer.XmlTransformer;

public class ViewElementService implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(XmlTransformer.class);

	private XmlTransformer<AbstractViewElement> transformer = null;
	private HashSet<AbstractViewElement> viewElements = new HashSet<>();

	public ViewElementService() {
		this("ViewElementPersistence.xml");
	}

	public ViewElementService(String xmlFile) {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(xmlFile).getFile());
		transformer = new XmlTransformer<AbstractViewElement>(file);
	}

	public AbstractViewElement putViewElement(AbstractViewElement ve) {
		try {
			if (ve != null) {

				if (ve.getId() == null)
					ve.setId(getHighestId() + 1);
				viewElements.remove(ve);
				viewElements.add(ve);
				saveViewModels();
			}
		} catch (Exception e) {
			LOGGER.error("save failed ", e);
		}
		return ve;
	}

	private void saveViewModels() {
		transformer.write(viewElements);
	}

	private void deleteViewModel(Long id) {
		if (viewElements == null || viewElements.isEmpty())
			refreshViewElements();
		
		for (AbstractViewElement ve : new HashSet<>(viewElements)) {

			deleteNestedElements(ve, id);

			if (ve.getId() != null && ve.getId().equals(id)) {
				viewElements.remove(ve);
				LOGGER.debug("ViewElement deleted id:" + ve.getId());
			}
		}

		transformer.write(viewElements);
	}

	private void deleteNestedElements(AbstractViewElement viewElement, Long idToDelete) {
		if (viewElement != null && viewElement.getNestedElements() != null) {

			for (Long nested : new ArrayList<>(viewElement.getNestedElements())) {

				if (nested != null && nested.equals(idToDelete)) {

					viewElement.getNestedElements().remove(nested);
					viewElements.add(viewElement);
				}
			}
		}
	}

	public void deleteViewModel(AbstractViewElement viewElement) {
		deleteViewModel(viewElement.getId());
	}

	public AbstractViewElement findViewElement(Long id) {
		
		if (viewElements == null || viewElements.isEmpty())
			refreshViewElements();
		
		for (AbstractViewElement ve : viewElements) {
			if (ve.getId() != null && ve.getId().equals(id))
				return ve;
		}

		return null;
	}

	public List<AbstractViewElement> findViewElementByClass(String className) {
		List<AbstractViewElement> filteredViewElements = new ArrayList<>();

		if (viewElements == null || viewElements.isEmpty())
			refreshViewElements();
		
		for (AbstractViewElement ve : viewElements) {
			if (ve.getId() != null && ve.getClass().getName().equals(className))
				filteredViewElements.add(ve);
		}

		return filteredViewElements;
	}

	public HashSet<AbstractViewElement> getViewElements() {
		refreshViewElements();
		return viewElements;
	}

	private void refreshViewElements() {
		viewElements = (HashSet<AbstractViewElement>) transformer.read();
	}

	private Long getHighestId() {
		Long highestIdTemp = new Long(0);

		for (AbstractViewElement ve : getViewElements()) {
			if (ve.getId() != null && ve.getId() > highestIdTemp)
				highestIdTemp++;
		}

		LOGGER.debug("Highest ID = " + highestIdTemp);
		return highestIdTemp;
	}

}
