package ch.ultrasoft.guieditorjsf.xmltransformer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import org.apache.log4j.Logger;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.StreamException;

public class XmlTransformer<T> {

	private static final Logger LOGGER = Logger.getLogger(XmlTransformer.class);

	private static XStream stream = new XStream();
	private File xmlFile = null;

	public XmlTransformer(File xmlFile) {
		this.xmlFile = xmlFile;
		stream.setMode(XStream.ID_REFERENCES);
	}

	@SuppressWarnings("unchecked")
	public Set<T> read() {

		LOGGER.debug("XmlTransformer read");
		try {
			debugXMLFileContent();
			return (Set<T>) stream.fromXML(new FileReader(xmlFile));
		} catch (IOException | StreamException e) {
			LOGGER.error("fail to read from XML " + xmlFile.getAbsolutePath());
			LOGGER.debug(e);
		}
		return new HashSet<T>();
	}

	public void write(Set<T> objects) {

		LOGGER.debug("XmlTransformer write");
		try {
			stream.toXML(objects, new FileWriter(xmlFile));
			debugXMLFileContent();
		} catch (IOException e) {
			LOGGER.error("fail writing to XML" + xmlFile.getAbsolutePath(), e);
		}
	}

	private void debugXMLFileContent() {
		if (LOGGER.isDebugEnabled()) {
			try {
				StringBuilder stringBuilder = new StringBuilder();
				Scanner s = new Scanner(xmlFile);
				while (s.hasNextLine()) {
					stringBuilder.append("\n" + s.nextLine());
				}
				s.close();
				LOGGER.debug(stringBuilder);
			} catch (FileNotFoundException e) {
				LOGGER.error("fail: debugXMLFileContent " + xmlFile.getAbsolutePath(), e);
			}
		}
	}
}
