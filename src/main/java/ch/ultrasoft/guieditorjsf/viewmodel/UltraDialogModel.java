package ch.ultrasoft.guieditorjsf.viewmodel;

import java.util.List;

/**
 * @author manigley
 */

//@Entity
//@Table(name = "DialogModel")
//@PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
public class UltraDialogModel extends AbstractViewElement {

	private static final long serialVersionUID = 1L;

//	@Column(name = "formId", nullable = false)
	private String formId;
	
//	@Column(name = "widgetWar", nullable = false)
	private String widgetWar;
	
//	@Column(name = "header", nullable = false)
	private String header;

//	@Column(name = "dialogModelName")
	private String dialogModelName;

//	@Column(name = "modal", nullable = false)
	private Boolean modal = true;

	public UltraDialogModel() {
		
	}
	
	public UltraDialogModel(String formId, String widgetWar, String header, Boolean modal,
			List<Long> nestedElements) {
		super(nestedElements, "DialogModel");
		this.formId = formId;
		this.widgetWar = widgetWar;
		this.header = header;
		this.modal = modal;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getWidgetWar() {
		return widgetWar;
	}

	public void setWidgetWar(String widgetWar) {
		this.widgetWar = widgetWar;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getDialogModelName() {
		return dialogModelName;
	}

	public void setDialogModelName(String dialogModelName) {
		this.dialogModelName = dialogModelName;
	}

	public Boolean getModal() {
		return modal;
	}

	public void setModal(Boolean modal) {
		this.modal = modal;
	}
}
