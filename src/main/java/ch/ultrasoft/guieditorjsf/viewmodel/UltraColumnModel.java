package ch.ultrasoft.guieditorjsf.viewmodel;

import java.util.List;

/**
 * @author manigley
 */

//@Entity
//@Table(name = "ColumnModel")
//@PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
public class UltraColumnModel extends AbstractViewElement {

	private static final long serialVersionUID = 1L;

//	@Column(name = "header")
	private String header;

	public UltraColumnModel() {
	}

	public UltraColumnModel(String header, List<Long> nestedElements, String type) {
		super(nestedElements, type);
		this.header = header;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}
}