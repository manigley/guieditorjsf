package ch.ultrasoft.guieditorjsf.viewmodel;

import java.util.ArrayList;

/**
 * @author manigley
 */

//@Entity
//@Table(name = "InputTextModel")
//@PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
public class UltraInputTextModel extends AbstractViewElement {

	private static final long serialVersionUID = 1L;
	
//	@Column(name = "value")
	private String value;
//	@Column(name = "bean")
	private String bean;

	public UltraInputTextModel() {
	}
	
	public UltraInputTextModel(String value, String bean) {
		super(new ArrayList<Long>(), "InputTextModel");
		this.value = value;
		this.bean = bean;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getBean() {
		return bean;
	}

	public void setBean(String bean) {
		this.bean = bean;
	}
}
