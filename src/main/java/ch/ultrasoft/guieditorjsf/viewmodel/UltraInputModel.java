package ch.ultrasoft.guieditorjsf.viewmodel;

import java.util.ArrayList;

/**
 * @author manigley
 */

//@Entity
//@Table(name = "InputModel")
//@PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
public class UltraInputModel extends AbstractViewElement {

	private static final long serialVersionUID = 1L;

//	@Column(name = "label", nullable = false)
	private String label;
	
//	@Column(name = "max", nullable = false)
	private Integer max;
	
//	@Column(name = "min", nullable = false)
	private Integer min;
	
//	@Column(name = "maxLength", nullable = false)
	private Integer maxLength;
	
//	@Column(name = "sortierung", nullable = false)
	private Integer sortierung;
	
//	@ManyToOne(targetEntity = AbstractViewElement.class)
//	@JoinColumn(name = "AbstractViewElement_FK")
	private AbstractViewElement element = new UltraInputTextModel("role", "roleController");
	
	public UltraInputModel(){
	}
	
	public UltraInputModel(String label, Integer max, Integer min, Integer maxLength, Integer sortierung, AbstractViewElement element) {
		super(new ArrayList<Long>(), "InputModel");
		this.label = label;
		this.max = max;
		this.min = min;
		this.maxLength = maxLength;
		this.sortierung = sortierung;
		this.element = element;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Integer getMax() {
		return max;
	}
	public void setMax(Integer  max) {
		this.max = max;
	}
	public Integer  getMin() {
		return min;
	}
	public void setMin(Integer  min) {
		this.min = min;
	}
	public Integer getMaxLength() {
		return maxLength;
	}
	public void setMaxLength(Integer maxLength) {
		this.maxLength = maxLength;
	}
	public Integer getSortierung() {
		return sortierung;
	}
	public void setSortierung(Integer sortierung) {
		this.sortierung = sortierung;
	}

	public AbstractViewElement getElement() {
		return element;
	}

	public void setElement(AbstractViewElement element) {
		this.element = element;
	}
}
