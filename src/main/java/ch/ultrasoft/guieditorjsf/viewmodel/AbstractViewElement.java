package ch.ultrasoft.guieditorjsf.viewmodel;

import java.io.Serializable;
import java.util.List;

/**
 * @author manigley
 *
 * Hibernate Mapping
 * 
 * https://docs.jboss.org/hibernate/stable/annotations/reference/en/html_single/
 * #d0e1168
 */
//@Entity
//@Table(name = "AbstractViewElement")
//@Inheritance(strategy = InheritanceType.JOINED)
public abstract class AbstractViewElement implements Serializable {
	private static final long serialVersionUID = 1L;

//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Basic(optional = false)
//	@Column(name = "id")
	private Long id;

//	@Column(name = "idElement")
	protected String idElement;

//	@Column(name = "type")
	protected String type;

//	@ManyToMany
//    @JoinTable(name="ViewModelRelation", joinColumns={@JoinColumn(name="id_master")},inverseJoinColumns={@JoinColumn(name="id_slave")})
	private List<Long> nestedElements;

	public AbstractViewElement(){
		
	}
	
	public AbstractViewElement(List<Long> nestedElements, String type) {
		this.nestedElements = nestedElements;
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIdElement() {
		return idElement;
	}

	public void setIdElement(String idElement) {
		this.idElement = idElement;
	}

	public List<Long> getNestedElements() {
		return nestedElements;
	}

	public void setNestedElements(List<Long> nestedElements) {
		this.nestedElements = nestedElements;
	}

	@Override
	public int hashCode() {
		return (int) (id + 1);
	}

	@Override
	public boolean equals(Object obj) {
		if (((AbstractViewElement)obj).getId() != null && ((AbstractViewElement)obj).getId().equals(this.id))
			return true;
		return false;
	}
	
	
}
