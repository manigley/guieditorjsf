/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.ultrasoft.guieditorjsf.viewmodel;

import java.io.Serializable;

/**
 * @author manigley
 * 
 * ManyToMany Mapping (Unidirectional)
 * 
 * https://howtoprogramwithjava.com/hibernate-manytomany-unidirectional-bidirectional/
 */

//@Entity
//@Table(name = "ViewModelRelation")
public class ViewModelRelation implements Serializable {
	private static final long serialVersionUID = 1L;

//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Basic(optional = false)
//	@Column(name = "id")
	private Integer id;

//	@ManyToOne
//	@JoinColumn(name = "id_master" , insertable = false, updatable = false, nullable = true)
	private AbstractViewElement master;

//	@ManyToOne
//	@JoinColumn(name = "id_slave", insertable = false, updatable = false, nullable = true)
	private AbstractViewElement slave;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AbstractViewElement getMaster() {
		return master;
	}

	public void setMaster(AbstractViewElement master) {
		this.master = master;
	}

	public AbstractViewElement getSlave() {
		return slave;
	}

	public void setSlave(AbstractViewElement slave) {
		this.slave = slave;
	}

}
