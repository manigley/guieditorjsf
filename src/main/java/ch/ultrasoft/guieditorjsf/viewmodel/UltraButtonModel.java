package ch.ultrasoft.guieditorjsf.viewmodel;

/**
 * @author manigley
 */

//@Entity
//@Table(name = "ButtonModel")
//@PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
public class UltraButtonModel extends AbstractViewElement {

	private static final long serialVersionUID = 1L;

//	@Column(name = "bean")
	private String bean;

//	@Column(name = "methodName")
	private String methodName;

//	@Column(name = "parameter")
	private boolean parameter;

//	@Column(name = "value")
	private String value;

//	@Column(name = "icon")
	private String icon;

//	@Column(name = "onComplete")
	private String onComplete;

//	@Column(name = "onClick")
	private String onClick;

//	@Column(name = "updateElement")
	private String update;

	public UltraButtonModel() {
	}

	public UltraButtonModel(String bean, String methodName, boolean parameter, String value, String icon,
			String onComplete, String onClick, String update) {
		super();
		this.bean = bean;
		this.methodName = methodName;
		this.parameter = parameter;
		this.value = value;
		this.icon = icon;
		this.onComplete = onComplete;
		this.onClick = onClick;
		this.update = update;
	}

	public String getBean() {
		return bean;
	}

	public void setBean(String bean) {
		this.bean = bean;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public boolean isParameter() {
		return parameter;
	}

	public void setParameter(boolean parameter) {
		this.parameter = parameter;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getOnComplete() {
		return onComplete;
	}

	public void setOnComplete(String onComplete) {
		this.onComplete = onComplete;
	}

	public String getOnClick() {
		return onClick;
	}

	public void setOnClick(String onClick) {
		this.onClick = onClick;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "UltraButtonModel";
	}
}
