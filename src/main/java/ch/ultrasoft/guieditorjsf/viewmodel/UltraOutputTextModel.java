package ch.ultrasoft.guieditorjsf.viewmodel;

/**
 * @author manigley
 */

//@Entity
//@Table(name = "OutputTextModel")
public class UltraOutputTextModel extends AbstractViewElement {

	private static final long serialVersionUID = 1L;
	
//	@Column(name = "value")
	private String value;

	public UltraOutputTextModel() {
	}

	public UltraOutputTextModel(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
