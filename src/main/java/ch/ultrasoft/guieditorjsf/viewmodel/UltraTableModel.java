package ch.ultrasoft.guieditorjsf.viewmodel;

import java.io.Serializable;
import java.util.List;

/**
 * @author manigley
 */

//@Entity
//@Table(name = "TableModel")
//@PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
public class UltraTableModel extends AbstractViewElement implements Serializable {
	
	private static final long serialVersionUID = 1L;

//	@Column(name="bean", nullable = false)
	private String bean;
//	@Column(name="listName", nullable = false)
	private String listName;
//	@Column(name="tableModelName")
	private String tableModelName;

	public UltraTableModel() {
	}

	public UltraTableModel(List<Long> nestedElements, String bean, String listName) {
		super(nestedElements, "dataTable");
		this.bean = bean;
		this.listName = listName;
	}

	public String getBean() {
		return bean;
	}

	public void setBean(String bean) {
		this.bean = bean;
	}

	public String getListName() {
		return listName;
	}

	public void setListName(String listName) {
		this.listName = listName;
	}

	public String getTableModelName() {
		return tableModelName;
	}

	public void setTableModelName(String tableModelName) {
		this.tableModelName = tableModelName;
	}
}
