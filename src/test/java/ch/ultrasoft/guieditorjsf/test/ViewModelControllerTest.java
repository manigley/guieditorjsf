package ch.ultrasoft.guieditorjsf.test;

import org.junit.Test;

import ch.ultrasoft.guieditorjsf.viewcontroller.ViewModelController;
import ch.ultrasoft.guieditorjsf.viewmodel.AbstractViewElement;

public class ViewModelControllerTest {

	private final ViewModelController controller = new ViewModelController();

	@Test
	public void test() {
		readTest();
	}

	private void readTest() {
		for (AbstractViewElement ve : controller.getViewElements()) {
//			if (ve.getNestedElements() != null && !ve.getNestedElements().isEmpty())
//				for (AbstractViewElement nested : ve.getNestedElements()) {
//					System.out.println("\tNESTED ID: " + nested.getId());
//				}
			System.out.println("ID: " + ve.getId());
			System.out.println("find by id: " + controller.getService().findViewElement(new Long(66)));
			controller.setClassNameNewElement("UltraColumnModel");
			System.out.println("find by class size: " + controller.getViewElementsByClassName().size());
			System.out.println(controller.getViewElements());
		}
	}

}
