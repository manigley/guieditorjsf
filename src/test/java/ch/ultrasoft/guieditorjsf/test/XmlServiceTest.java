package ch.ultrasoft.guieditorjsf.test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import ch.ultrasoft.guieditorjsf.viewmodel.AbstractViewElement;
import ch.ultrasoft.guieditorjsf.viewmodel.UltraButtonModel;
import ch.ultrasoft.guieditorjsf.viewmodel.UltraColumnModel;
import ch.ultrasoft.guieditorjsf.xmlservice.ViewElementService;

public class XmlServiceTest {

	private ViewElementService service = new ViewElementService("ViewElementPersistenceTest.xml");

	private UltraButtonModel button = new UltraButtonModel("testBean", "test", false, "Hello", "fa fa-smile", "", "",
			"");

	private UltraColumnModel column = new UltraColumnModel("Column", null, "0815");
	private UltraColumnModel column2 = new UltraColumnModel("Column2", null, "0815");

	@Test
	public void test() {
		List<Long> nestedElements = new ArrayList<>();

		button = (UltraButtonModel) service.putViewElement(button);
		nestedElements.add(button.getId());

		button.setId(null);
		button = (UltraButtonModel) service.putViewElement(button);
		nestedElements.add(button.getId());

		column.setNestedElements(nestedElements);
		service.putViewElement(column);

		nestedElements.add(button.getId());
		nestedElements.add(button.getId());
		nestedElements.add(button.getId());
		nestedElements.add(button.getId());
		nestedElements.add(button.getId());
		column2.setNestedElements(nestedElements);
		service.putViewElement(column2);

	}

}
