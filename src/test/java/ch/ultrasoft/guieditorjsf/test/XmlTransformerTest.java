package ch.ultrasoft.guieditorjsf.test;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import ch.ultrasoft.guieditorjsf.viewmodel.AbstractViewElement;
import ch.ultrasoft.guieditorjsf.viewmodel.UltraButtonModel;
import ch.ultrasoft.guieditorjsf.xmltransformer.XmlTransformer;

public class XmlTransformerTest {

	private XmlTransformer<AbstractViewElement> transformer = null;
	
	private Set<AbstractViewElement> viewElementSet = new HashSet<>();
	private UltraButtonModel button = new UltraButtonModel("testBean", "test", false, "Hello", "fa fa-smile", "", "", "");
	
	@Test
	public void test() {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("ViewElementPersistenceTest.xml").getFile());
		
		transformer = new XmlTransformer<AbstractViewElement>(file);
		button.setId(new Long(55));
		viewElementSet.add(button);
		transformer.write(viewElementSet);
		transformer.read();
	}
	
	

}
