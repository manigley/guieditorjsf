package ch.ultrasoft.guieditorjsf.test;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import ch.ultrasoft.guieditorjsf.viewmodel.AbstractViewElement;
import ch.ultrasoft.guieditorjsf.viewmodel.UltraColumnModel;
import ch.ultrasoft.guieditorjsf.xmltransformer.XmlTransformer;

public class XmlTransformerTestObjectRef {

	private XmlTransformer<AbstractViewElement> transformer = null;

	private Set<AbstractViewElement> viewElementSet = new HashSet<>();
	private UltraColumnModel col1 = new UltraColumnModel("Header1", null, "0");
	private UltraColumnModel col2 = new UltraColumnModel("Header2", null, "0");

	@Test
	public void test() {
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource("ViewElementPersistenceTest.xml").getFile());
			
			transformer = new XmlTransformer<AbstractViewElement>(file);
			col1.setId(new Long(1));
			col2.setId(new Long(2));

			List<Long> nested = new ArrayList<>();
			for (Long i = new Long(10); i < 15; i++) {
				UltraColumnModel nestedCol = new UltraColumnModel("nested" + i, null, "0");
				nestedCol.setId(i);
				nested.add(nestedCol.getId());
			}
			col1.setNestedElements(new ArrayList<>(nested));
			System.out.println(nested.size());
			nested.remove(2);
			col2.setNestedElements(nested);
			viewElementSet.add(col1);
			viewElementSet.add(col2);
			
			transformer.write(viewElementSet);
			
			col2.getNestedElements().get(1);
			viewElementSet.add(col2);
			
			transformer.write(viewElementSet);
			
			col2.getNestedElements().get(2);
			UltraColumnModel newCol = new UltraColumnModel("HHHHHEEEEEAAAADDDEEEERRR", null, "0");
			newCol.setId(new Long(88));
			nested.add(newCol.getId());
			col2.getNestedElements().addAll(nested);
			
			viewElementSet.add(col2);
			col2.setId(new Long(99));
			viewElementSet.add(col2);
			transformer.write(viewElementSet);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
